<?php

namespace App\Tests;

use App\Service\Helper;
use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Config\Definition\Exception\InvalidTypeException;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;
use Symfony\Component\Filesystem\Filesystem;

class HelperTest extends WebTestCase
{
    public function testSuccessIsJson(): void
    {
        $data = '{
            "Result": "OK",
            "Data": [
              {
                "Sku_Provider": "10-1001SS",
                "Ean": "3701405800055,3701405800062,3701405800079,8414614010010",
                "Provider_Full_Description": "PELUCHE MARIONETAS CANTARINAS BABY SHARK MOD SDOS",
                "Provider_Name": "PELUCHE MARIONETAS CANTARINAS BABY SHARK MOD SDOS",
                "Brand_Supplier_Name": "Bandai",
                "Category_Supplier_Name": "Outlet",
                "Width_Packaging": 20.00,
                "Height_Packaging": 24.00,
                "Length_Packaging": 15.00,
                "Weight_Packaging": 0.06,
                "New": false,
                "Active": true,
                "Attributes": [
                  {
                    "Attribute_ID": "1",
                    "Attribute_Name": "Edad Minima Recomendada",
                    "Attribute_Value": "3A"
                  },
                  {
                    "Attribute_ID": "5",
                    "Attribute_Name": "Utiliza Pilas",
                    "Attribute_Value": "S"
                  },
                  {
                    "Attribute_ID": "6",
                    "Attribute_Name": "Tipo Pilas",
                    "Attribute_Value": "LR44"
                  },
                  {
                    "Attribute_ID": "7",
                    "Attribute_Name": "Incluye Pilas",
                    "Attribute_Value": "S"
                  },
                  {
                    "Attribute_ID": "8",
                    "Attribute_Name": "Emite Sonidos",
                    "Attribute_Value": "S"
                  },
                  {
                    "Attribute_ID": "9",
                    "Attribute_Name": "Tiene Luces",
                    "Attribute_Value": "N"
                  },
                  {
                    "Attribute_ID": "17",
                    "Attribute_Name": "Número Pilas",
                    "Attribute_Value": "3"
                  }
                ],
                "Images": [
                  "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-316539-800-600.jpg",
                  "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-321983-800-600.jpg",
                  "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-353017-800-600.jpg",
                  "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-353018-800-600.jpg",
                  "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-353019-800-600.jpg",
                  "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-353020-800-600.jpg",
                  "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-353021-800-600.jpg",
                  "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-353022-800-600.jpg",
                  "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-353023-800-600.jpg",
                  "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-353024-800-600.jpg",
                  "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-310532-800-600.jpg",
                  "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-310533-800-600.jpg",
                  "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-310534-800-600.jpg",
                  "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-316535-800-600.jpg",
                  "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-316536-800-600.jpg",
                  "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-316537-800-600.jpg",
                  "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-316538-800-600.jpg"
                ]
              },
              {
                "Sku_Provider": "10-1100SS",
                "Ean": "3701405800086",
                "Provider_Full_Description": "Figuras de baño Baby Shark, Pack 3 unidades",
                "Provider_Name": "Figuras de baño Baby Shark, Pack 3 unidades",
                "Intrastat": "95030049",
                "Brand_Supplier_Name": "Bandai",
                "Category_Supplier_Name": "Juguetes primera infancia",
                "Width_Packaging": 51.00,
                "Height_Packaging": 13.00,
                "Length_Packaging": 20.00,
                "Weight_Packaging": 0.06,
                "New": false,
                "Active": false,
                "Attributes": [
                  {
                    "Attribute_ID": "1",
                    "Attribute_Name": "Edad Minima Recomendada",
                    "Attribute_Value": "2a"
                  },
                  {
                    "Attribute_ID": "5",
                    "Attribute_Name": "Utiliza Pilas",
                    "Attribute_Value": "N"
                  }
                ],
                "Images": [
                  "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1100SS-353443-800-600.jpg",
                  "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1100SS-310457-800-600.jpg",
                  "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1100SS-310458-800-600.jpg",
                  "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1100SS-310459-800-600.jpg",
                  "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1100SS-353030-800-600.jpg",
                  "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1100SS-353031-800-600.jpg",
                  "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1100SS-353442-800-600.jpg"
                ]
              }
            ]
          }';
          
          $fileSystem = new Filesystem();
          $helper = new Helper($fileSystem);

          $this->assertEquals(true, $helper->isJson($data));

    }

    public function testFailIsJson(): void
    {
        $data = '"Result": "OK",
            "Data": [
              {
                "Sku_Provider": "10-1001SS",
                "Ean": "3701405800055,3701405800062,3701405800079,8414614010010",
                "Provider_Full_Description": "PELUCHE MARIONETAS CANTARINAS BABY SHARK MOD SDOS",
                "Provider_Name": "PELUCHE MARIONETAS CANTARINAS BABY SHARK MOD SDOS",
                "Brand_Supplier_Name": "Bandai",
                "Category_Supplier_Name": "Outlet",
                "Width_Packaging": 20.00,
                "Height_Packaging": 24.00,
                "Length_Packaging": 15.00,
                "Weight_Packaging": 0.06,
                "New": false,
                "Active": true,
                "Attributes": [
                  {
                    "Attribute_ID": "1",
                    "Attribute_Name": "Edad Minima Recomendada",
                    "Attribute_Value": "3A"
                  },
                  {
                    "Attribute_ID": "5",
                    "Attribute_Name": "Utiliza Pilas",
                    "Attribute_Value": "S"
                  },
                  {
                    "Attribute_ID": "6",
                    "Attribute_Name": "Tipo Pilas",
                    "Attribute_Value": "LR44"
                  },
                  {
                    "Attribute_ID": "7",
                    "Attribute_Name": "Incluye Pilas",
                    "Attribute_Value": "S"
                  },
                  {
                    "Attribute_ID": "8",
                    "Attribute_Name": "Emite Sonidos",
                    "Attribute_Value": "S"
                  },
                  {
                    "Attribute_ID": "9",
                    "Attribute_Name": "Tiene Luces",
                    "Attribute_Value": "N"
                  },
                  {
                    "Attribute_ID": "17",
                    "Attribute_Name": "Número Pilas",
                    "Attribute_Value": "3"
                  }
                ],
                "Images": [
                  "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-316539-800-600.jpg",
                  "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-321983-800-600.jpg",
                  "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-353017-800-600.jpg",
                  "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-353018-800-600.jpg",
                  "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-353019-800-600.jpg",
                  "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-353020-800-600.jpg",
                  "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-353021-800-600.jpg",
                  "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-353022-800-600.jpg",
                  "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-353023-800-600.jpg",
                  "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-353024-800-600.jpg",
                  "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-310532-800-600.jpg",
                  "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-310533-800-600.jpg",
                  "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-310534-800-600.jpg",
                  "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-316535-800-600.jpg",
                  "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-316536-800-600.jpg",
                  "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-316537-800-600.jpg",
                  "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-316538-800-600.jpg"
                ]
              }
              ]
        }';

        $fileSystem = new Filesystem();
        $helper = new Helper($fileSystem);

        $this->expectException(InvalidTypeException::class);
        $helper->isJson($data);

    }

    public function testSuccessSaveJson(): void
    {

      $data = '{
        "Result": "OK",
        "Data": [
          {
            "Sku_Provider": "10-1001SS",
            "Ean": "3701405800055,3701405800062,3701405800079,8414614010010",
            "Provider_Full_Description": "PELUCHE MARIONETAS CANTARINAS BABY SHARK MOD SDOS",
            "Provider_Name": "PELUCHE MARIONETAS CANTARINAS BABY SHARK MOD SDOS",
            "Brand_Supplier_Name": "Bandai",
            "Category_Supplier_Name": "Outlet",
            "Width_Packaging": 20.00,
            "Height_Packaging": 24.00,
            "Length_Packaging": 15.00,
            "Weight_Packaging": 0.06,
            "New": false,
            "Active": true,
            "Attributes": [
              {
                "Attribute_ID": "1",
                "Attribute_Name": "Edad Minima Recomendada",
                "Attribute_Value": "3A"
              },
              {
                "Attribute_ID": "5",
                "Attribute_Name": "Utiliza Pilas",
                "Attribute_Value": "S"
              },
              {
                "Attribute_ID": "6",
                "Attribute_Name": "Tipo Pilas",
                "Attribute_Value": "LR44"
              },
              {
                "Attribute_ID": "7",
                "Attribute_Name": "Incluye Pilas",
                "Attribute_Value": "S"
              },
              {
                "Attribute_ID": "8",
                "Attribute_Name": "Emite Sonidos",
                "Attribute_Value": "S"
              },
              {
                "Attribute_ID": "9",
                "Attribute_Name": "Tiene Luces",
                "Attribute_Value": "N"
              },
              {
                "Attribute_ID": "17",
                "Attribute_Name": "Número Pilas",
                "Attribute_Value": "3"
              }
            ],
            "Images": [
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-316539-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-321983-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-353017-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-353018-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-353019-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-353020-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-353021-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-353022-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-353023-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-353024-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-310532-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-310533-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-310534-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-316535-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-316536-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-316537-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-316538-800-600.jpg"
            ]
          },
          {
            "Sku_Provider": "10-1100SS",
            "Ean": "3701405800086",
            "Provider_Full_Description": "Figuras de baño Baby Shark, Pack 3 unidades",
            "Provider_Name": "Figuras de baño Baby Shark, Pack 3 unidades",
            "Intrastat": "95030049",
            "Brand_Supplier_Name": "Bandai",
            "Category_Supplier_Name": "Juguetes primera infancia",
            "Width_Packaging": 51.00,
            "Height_Packaging": 13.00,
            "Length_Packaging": 20.00,
            "Weight_Packaging": 0.06,
            "New": false,
            "Active": false,
            "Attributes": [
              {
                "Attribute_ID": "1",
                "Attribute_Name": "Edad Minima Recomendada",
                "Attribute_Value": "2a"
              },
              {
                "Attribute_ID": "5",
                "Attribute_Name": "Utiliza Pilas",
                "Attribute_Value": "N"
              }
            ],
            "Images": [
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1100SS-353443-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1100SS-310457-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1100SS-310458-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1100SS-310459-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1100SS-353030-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1100SS-353031-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1100SS-353442-800-600.jpg"
            ]
          }
        ]
      }';

      $jsonDir = $this->bootKernel()->getProjectDir().'/public/uploads/json';
      $jsonFile = $jsonDir . '/file.json';

      /**
       * @var Filesystem&MockObject $filesystem
       */
      $filesystem = $this->getMockBuilder(Filesystem::class)
          ->disableOriginalConstructor()
          ->getMock();
      
      $filesystem
          ->expects(self::exactly(1))
          ->method('dumpFile')
          ->with($jsonFile, $data);

      $helper = new Helper($filesystem);
          
      $response = $helper->saveJson($data, $jsonDir);

      $this->assertSame(200, $response->getStatusCode());

    }

    public function testFailSaveJson(): void
    {

      $data = '{
        "Result": "OK",
        "Data": [
          {
            "Sku_Provider": "10-1001SS",
            "Ean": "3701405800055,3701405800062,3701405800079,8414614010010",
            "Provider_Full_Description": "PELUCHE MARIONETAS CANTARINAS BABY SHARK MOD SDOS",
            "Provider_Name": "PELUCHE MARIONETAS CANTARINAS BABY SHARK MOD SDOS",
            "Brand_Supplier_Name": "Bandai",
            "Category_Supplier_Name": "Outlet",
            "Width_Packaging": 20.00,
            "Height_Packaging": 24.00,
            "Length_Packaging": 15.00,
            "Weight_Packaging": 0.06,
            "New": false,
            "Active": true,
            "Attributes": [
              {
                "Attribute_ID": "1",
                "Attribute_Name": "Edad Minima Recomendada",
                "Attribute_Value": "3A"
              },
              {
                "Attribute_ID": "5",
                "Attribute_Name": "Utiliza Pilas",
                "Attribute_Value": "S"
              },
              {
                "Attribute_ID": "6",
                "Attribute_Name": "Tipo Pilas",
                "Attribute_Value": "LR44"
              },
              {
                "Attribute_ID": "7",
                "Attribute_Name": "Incluye Pilas",
                "Attribute_Value": "S"
              },
              {
                "Attribute_ID": "8",
                "Attribute_Name": "Emite Sonidos",
                "Attribute_Value": "S"
              },
              {
                "Attribute_ID": "9",
                "Attribute_Name": "Tiene Luces",
                "Attribute_Value": "N"
              },
              {
                "Attribute_ID": "17",
                "Attribute_Name": "Número Pilas",
                "Attribute_Value": "3"
              }
            ],
            "Images": [
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-316539-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-321983-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-353017-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-353018-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-353019-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-353020-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-353021-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-353022-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-353023-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-353024-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-310532-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-310533-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-310534-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-316535-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-316536-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-316537-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1001SS-316538-800-600.jpg"
            ]
          },
          {
            "Sku_Provider": "10-1100SS",
            "Ean": "3701405800086",
            "Provider_Full_Description": "Figuras de baño Baby Shark, Pack 3 unidades",
            "Provider_Name": "Figuras de baño Baby Shark, Pack 3 unidades",
            "Intrastat": "95030049",
            "Brand_Supplier_Name": "Bandai",
            "Category_Supplier_Name": "Juguetes primera infancia",
            "Width_Packaging": 51.00,
            "Height_Packaging": 13.00,
            "Length_Packaging": 20.00,
            "Weight_Packaging": 0.06,
            "New": false,
            "Active": false,
            "Attributes": [
              {
                "Attribute_ID": "1",
                "Attribute_Name": "Edad Minima Recomendada",
                "Attribute_Value": "2a"
              },
              {
                "Attribute_ID": "5",
                "Attribute_Name": "Utiliza Pilas",
                "Attribute_Value": "N"
              }
            ],
            "Images": [
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1100SS-353443-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1100SS-310457-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1100SS-310458-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1100SS-310459-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1100SS-353030-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1100SS-353031-800-600.jpg",
              "http://intranet.grupotoysmaniatic.es:8080/ArticuloImagen/Index/10_1100SS-353442-800-600.jpg"
            ]
          }
        ]';

      $jsonDir = $this->bootKernel()->getProjectDir().'/public/uploads/json';
          
      $fileSystem = new Filesystem();
      $helper = new Helper($fileSystem);
      
      $this->expectException(InvalidTypeException::class);
      $response = $helper->saveJson($data, $jsonDir);

    }

}
