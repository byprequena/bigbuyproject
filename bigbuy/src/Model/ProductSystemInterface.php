<?php

namespace App\Model;

interface ProductSystemInterface
{
    public function setSku(string $sku);

    public function getSku(): ?string;

    public function setEan13(?string $ean13);

    public function getEan13(): ?string;

    public function setStock(int $stock);

    public function getStock(): ?int;

    public function setStockCatalog(int $stockCatalog);

    public function getStockCatalog(): ?int;

    public function setStockToShow(int $stockToShow);

    public function getStockToShow(): int;

    public function setStockAvailable(int $stockAvailable);

    public function getStockAvailable(): int;

    public function setCategoryName(?string $categoryName);

    public function getCategoryName(): ?string;

    public function setBrandName(?string $brandName);

    public function getBrandName(): ?string;

    public function setPriceCatalog(float $priceCatalog);

    public function getPriceCatalog(): ?float;

    public function setPriceWholesale(?float $priceWholesale);

    public function getPriceWholesale(): ?float;

    public function getWeightPackaging(): ?string;

    public function getLengthPackaging(): ?string;

    public function getHeightPackaging(): ?string;

    public function getWidthPackaging(): ?string;

    public function setDescription(?string $description);

    public function getDescription(): ?string;

    public function setProductImages(array $productImages);

    public function getProductImages(): array;

    public function setProductAttributes($productAttributes);

    public function getProductAttributes();

}