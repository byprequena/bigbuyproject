<?php

namespace App\Repository;

use App\Entity\ProductSystem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProductSystem|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductSystem|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductSystem[]    findAll()
 * @method ProductSystem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductSystemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductSystem::class);
    }

    public function save(ProductSystem $productSystem): ProductSystem
    {
        $this->getEntityManager()->persist($productSystem);
        $this->getEntityManager()->flush();
        return $productSystem;
    }

    public function delete(ProductSystem $productSystem)
    {
        $this->getEntityManager()->remove($productSystem);
        $this->getEntityManager()->flush();
    }

    public function update(ProductSystem $productSystem): ProductSystem
    {
        $this->getEntityManager()->flush();
        return $productSystem;
    }

    public function getArraySkusMd5Bd(): array {

        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            'SELECT p.md5Attributes
            FROM App\Entity\ProductSystem p'
        );

        $arraySkuMd5 = [];

        foreach($query->getResult() as $product) {
            array_push($arraySkuMd5, $product['md5Attributes']);
        }

        $arraySkuMd5 = call_user_func_array('array_merge', $arraySkuMd5);

        // returns an array of Product objects
        return $arraySkuMd5;

    }
}
