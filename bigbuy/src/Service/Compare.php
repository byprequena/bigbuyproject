<?php

namespace App\Service;

use App\Repository\ProductSystemRepository;

class Compare {

    private $productsSystem;
    private $productSystemRepository;

    public function __construct(Array $productsSystem, ProductSystemRepository $productSystemRepository) {

        $this->productsSystem = $productsSystem;
        $this->productSystemRepository = $productSystemRepository;

    }

    private function getDifferences($arrayImported, $arrayBd) {
        return array_diff($arrayImported, $arrayBd);
    }

    public function saveDifferences($arrayImported, $arrayBd) {
        
        $differences = $this->getDifferences($arrayImported, $arrayBd);

        $cambios = [];
        foreach(array_keys($differences) as $sku) {

          $productSystem = $this->productSystemRepository->findOneBy(array('sku' => $sku));

          if($productSystem) {

            array_push($cambios, $sku);

            !empty(array_filter($this->productsSystem, function ( $obj ) use ($sku, $productSystem) {
                if($obj->getSku() == $sku) {
                  $this->productSystemRepository->delete($productSystem);
                  $this->productSystemRepository->save($obj);
                }
            }));

          }
          else {

            !empty(array_filter($this->productsSystem, function ( $obj ) use ($sku) {
                if($obj->getSku() == $sku) {
                  $this->productSystemRepository->save($obj);
                }
            }));
            
          }

        }

        if(count($cambios) > 0) {
          //NOTIFICAR CAMBIOS AQUI
        }

    }

}