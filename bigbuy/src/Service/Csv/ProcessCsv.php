<?php

namespace App\Service\Csv;

use App\Repository\ProductSystemRepository;
use App\Service\Compare;
use App\Util\TransformInterface;

class ProcessCsv {

    private $transformCsv;
    private $productSystemRepository;
    private $ftpServer;
    private $ftpUser;
    private $ftpPwd;
    private $ftpRelativePath;

    public function __construct(
      TransformInterface $transformCsv, 
      ProductSystemRepository $productSystemRepository,
      $ftpServer,
      $ftpUser,
      $ftpPwd,
      $ftpRelativePath
      )
    {
      $this->transformCsv = $transformCsv;
      $this->productSystemRepository = $productSystemRepository;
      $this->ftpServer = $ftpServer;
      $this->ftpUser = $ftpUser;
      $this->ftpPwd = $ftpPwd;
      $this->ftpRelativePath = $ftpRelativePath;
    }

    public function process() 
    {
        try {

          $csv = file_get_contents("ftp://$this->ftpUser:$this->ftpPwd@$this->ftpServer/$this->ftpRelativePath/Productos.csv");

          $productsSystemImported = $this->transformCsv->transform($csv);
          $arrayMd5Imported = $this->transformCsv->getArrayMd5();
          $arrayMd5Bd = $this->productSystemRepository->getArraySkusMd5Bd();

          $compare = new Compare($productsSystemImported, $this->productSystemRepository);
          $compare->saveDifferences($arrayMd5Imported, $arrayMd5Bd);
          
        }
        catch (\Exception $e) {
          throw $e;
        }
    }

}