<?php

namespace App\Service\Csv;

use App\Util\SerializerInterface;
use App\Util\TransformInterface as ServiceTransformInterface;

class TransformCsv implements ServiceTransformInterface {

    private $serializerCsv;
    public $arrayMd5;

    public function __construct(SerializerInterface $serializerCsv) {
        $this->serializerCsv = $serializerCsv;
        $this->arrayMd5 = [];
    }

    public function getArrayMd5(): Array {
        return $this->arrayMd5;
    }

    public function transform($csv): Array
    {
        $csvArray = str_getcsv($csv);

        $productsSystem = $this->serializerCsv->deserialize($csv);

        $this->addMd5($productsSystem, $csvArray);

        return $productsSystem;

    }

    public function addMd5(Array $productsSystem, Array $csvArray) 
    {
        $arrayMd5 = [];

        foreach($productsSystem as $k=>$productSystem) {
            $aux[$productSystem->getSku()] = md5(json_encode($csvArray[$k]));
            $productSystem->setMd5Attributes($aux);
            $arrayMd5[$productSystem->getSku()] = md5(json_encode($csvArray[$k]));
            unset($aux);
        }

        $this->arrayMd5 = $arrayMd5;
    }

}