<?php

namespace App\Service\Csv;

use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Serializer;
use App\Util\SerializerInterface;
use App\Service\Csv\AttributeCsvConverter;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class SerializerCsv implements SerializerInterface
{

    public function deserialize($csv): Array {

        $mapping = [
            'product_id'              => 'sku', 
            'Ean'                       => 'ean13', 
            'provider_short_description' => 'description',
            'Brand_Supplier_Name'       => 'brandName',
            'category_supplier_name'    => 'categoryName',
            'width_packaging'           => 'widthPackaging',
            'height_packaging'          => 'heightPackaging',
            'length_packaging'          => 'lengthPackaging',
            'weight_packaging'          => 'weightPackaging',
        ];

        $nameConverter = new AttributeCsvConverter($mapping);

        $normalizer = new ObjectNormalizer(null, $nameConverter);

        $serializer = new Serializer([$normalizer, new ArrayDenormalizer()], [new CsvEncoder()]);

        $context = array(
            CsvEncoder::DELIMITER_KEY => ';',
            CsvEncoder::KEY_SEPARATOR_KEY => ','
        );

        $productsSystem = $serializer->deserialize($csv, 'App\Entity\ProductSystem[]', 'csv', $context);

        return $productsSystem;

    }

}