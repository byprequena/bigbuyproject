<?php

namespace App\Service\Csv;

use Symfony\Component\Serializer\NameConverter\NameConverterInterface;

class AttributeCsvConverter implements NameConverterInterface
{
    public function __construct(array $mapping) {
        $this->mapping = $mapping;
    }

    public function denormalize($propertyName)
    {
        return $this->mapping[$propertyName];
    }

    public function normalize(string $propertyName)
    {
        return $propertyName;
    }

}