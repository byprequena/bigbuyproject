<?php

namespace App\Service\Json;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Serializer;
use App\Util\SerializerInterface;
use App\Service\Json\AttributeJsonConverter;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class SerializerJson implements SerializerInterface
{

    public function deserialize($json): Array {

        $mapping = [
            'Sku_Provider'              => 'sku', 
            'Ean'                       => 'ean13', 
            'Provider_Full_Description' => 'description',
            'Brand_Supplier_Name'       => 'brandName',
            'Category_Supplier_Name'    => 'categoryName',
            'Width_Packaging'           => 'widthPackaging',
            'Height_Packaging'          => 'heightPackaging',
            'Length_Packaging'          => 'lengthPackaging',
            'Weight_Packaging'          => 'weightPackaging',
            'Attributes'                => 'productAttributes',
            'Images'                    => 'productImages',
            'Provider_Name'             => '',
            'New'                       => '',
            'Active'                    => '',
            'Intrastat'                 => '',
        ];

        $nameConverter = new AttributeJsonConverter($mapping);

        $normalizer = new ObjectNormalizer(null, $nameConverter);

        $serializer = new Serializer([$normalizer, new ArrayDenormalizer()], [new JsonEncoder()]);

        $productsSystem = $serializer->deserialize($json, 'App\Entity\ProductSystem[]', 'json');

        return $productsSystem;

    }

}