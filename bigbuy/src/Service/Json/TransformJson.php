<?php

namespace App\Service\Json;

use App\Util\SerializerInterface;
use App\Util\TransformInterface as ServiceTransformInterface;

class TransformJson implements ServiceTransformInterface {

    private $serializerJson;
    public $arrayMd5;

    public function __construct(SerializerInterface $serializerJson) {
        $this->serializerJson = $serializerJson;
        $this->arrayMd5 = [];
    }

    public function getArrayMd5(): Array {
        return $this->arrayMd5;
    }

    public function transform($json): Array
    {
        $jsonArray  = json_decode($json, true);
        $jsonArray  = $jsonArray["Data"];
        $json       = json_encode($jsonArray);

        $productsSystem = $this->serializerJson->deserialize($json);

        $this->addMd5($productsSystem, $jsonArray);

        return $productsSystem;

    }

    public function addMd5(Array $productsSystem, Array $jsonArray) 
    {
        $arrayMd5 = [];

        foreach($productsSystem as $k=>$productSystem) {
            $aux[$productSystem->getSku()] = md5(json_encode($jsonArray[$k]));
            $productSystem->setMd5Attributes($aux);
            $arrayMd5[$productSystem->getSku()] = md5(json_encode($jsonArray[$k]));
            unset($aux);
        }

        $this->arrayMd5 = $arrayMd5;
    }

}