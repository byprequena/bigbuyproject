<?php

namespace App\Service\Json;

use App\Repository\ProductSystemRepository;
use App\Service\Compare;
use App\Service\Helper;
use App\Util\TransformInterface;
use Symfony\Component\Config\Definition\Exception\InvalidTypeException;

class ProcessJson {

    private $transformJson;
    private $productSystemRepository;
    private $helper;
    private $jsonDir;

    public function __construct(TransformInterface $transformJson, ProductSystemRepository $productSystemRepository, Helper $helper, $jsonDir)
    {
      $this->transformJson = $transformJson;
      $this->productSystemRepository = $productSystemRepository;
      $this->helper = $helper;
      $this->jsonDir = $jsonDir;
    }

    public function process() 
    {
        try {

            $path = $this->jsonDir.'/file.json';

            $json = file_get_contents($path);

            if( !$this->helper->isJson($json) ){
              throw new InvalidTypeException("The data is not a json");
            }

            $productsSystemImported = $this->transformJson->transform($json);
            $arrayMd5Imported = $this->transformJson->getArrayMd5();
            $arrayMd5Bd = $this->productSystemRepository->getArraySkusMd5Bd();
    
    
            $compare = new Compare($productsSystemImported, $this->productSystemRepository);
            $compare->saveDifferences($arrayMd5Imported, $arrayMd5Bd);
            
          }
          catch (\Exception $e) {
            throw $e;
          }
    }

}