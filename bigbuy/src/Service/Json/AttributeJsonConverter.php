<?php

namespace App\Service\Json;

use Symfony\Component\Serializer\NameConverter\NameConverterInterface;

class AttributeJsonConverter implements NameConverterInterface
{
    public function __construct(array $mapping) {
        $this->mapping = $mapping;
    }

    public function denormalize($propertyName)
    {
        return $this->mapping[$propertyName];
    }

    public function normalize(string $propertyName)
    {
        return $propertyName;
    }

}