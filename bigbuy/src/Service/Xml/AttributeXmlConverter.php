<?php

namespace App\Service\Xml;

use Symfony\Component\Serializer\NameConverter\NameConverterInterface;

class AttributeXmlConverter implements NameConverterInterface
{
    public function __construct(array $mapping) {
        $this->mapping = $mapping;
    }

    public function denormalize($propertyName)
    {
        return $this->mapping[$propertyName];
    }

    public function normalize(string $propertyName)
    {
        return $propertyName;
    }

}