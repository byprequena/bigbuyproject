<?php

namespace App\Service\Xml;

use App\Repository\ProductSystemRepository;
use App\Service\Compare;
use App\Util\TransformInterface;

class ProcessXml {

    private $transformXml;
    private $productSystemRepository;
    private $ftpServer;
    private $ftpUser;
    private $ftpPwd;
    private $ftpRelativePath;

    public function __construct(
      TransformInterface $transformXml, 
      ProductSystemRepository $productSystemRepository,
      $ftpServer,
      $ftpUser,
      $ftpPwd,
      $ftpRelativePath
      )
    {
      $this->transformXml = $transformXml;
      $this->productSystemRepository = $productSystemRepository;
      $this->ftpServer = $ftpServer;
      $this->ftpUser = $ftpUser;
      $this->ftpPwd = $ftpPwd;
      $this->ftpRelativePath = $ftpRelativePath;
    }

    public function process() 
    {
      try {

        $xml = file_get_contents("ftp://$this->ftpUser:$this->ftpPwd@$this->ftpServer/$this->ftpRelativePath/Articulos.xml");
        $xml = simplexml_load_string($xml);
        $json = json_encode($xml);

        $productsSystemImported = $this->transformXml->transform($json);
        $arrayMd5Imported = $this->transformXml->getArrayMd5();
        $arrayMd5Bd = $this->productSystemRepository->getArraySkusMd5Bd();

        $compare = new Compare($productsSystemImported, $this->productSystemRepository);
        $compare->saveDifferences($arrayMd5Imported, $arrayMd5Bd);
        
      }
      catch (\Exception $e) {
        throw $e;
      }
    }

}