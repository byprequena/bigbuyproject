<?php

namespace App\Service\Xml;

use App\Util\SerializerInterface;
use App\Util\TransformInterface as ServiceTransformInterface;

class TransformXml implements ServiceTransformInterface {

    private $serializerXml;
    public $arrayMd5;

    public function __construct(SerializerInterface $serializerXml) {
        $this->serializerXml = $serializerXml;
        $this->arrayMd5 = [];
    }

    public function getArrayMd5(): Array {
        return $this->arrayMd5;
    }

    public function transform($json): Array
    {
        $jsonArray  = json_decode($json, true);
        $jsonArray  = $jsonArray["Articulo"];
        $json       = json_encode($jsonArray);

        $productsSystem = $this->serializerXml->deserialize($json);

        $this->addMd5($productsSystem, $jsonArray);

        return $productsSystem;

    }

    public function addMd5(Array $productsSystem, Array $jsonArray) 
    {
        $arrayMd5 = [];

        foreach($productsSystem as $k=>$productSystem) {
            $aux[$productSystem->getSku()] = md5(json_encode($jsonArray[$k]));
            $productSystem->setMd5Attributes($aux);
            $arrayMd5[$productSystem->getSku()] = md5(json_encode($jsonArray[$k]));
            unset($aux);
        }

        $this->arrayMd5 = $arrayMd5;
    }

}