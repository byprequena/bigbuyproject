<?php

namespace App\Service\Xml;

use Symfony\Component\Serializer\Serializer;
use App\Util\SerializerInterface;
use App\Service\Xml\AttributeXmlConverter;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class SerializerXml implements SerializerInterface
{

    public function deserialize($json): Array {

        $mapping = [
            'Codigo'            => 'sku',
            'Descripcion'       => 'description',
            'CodigoBarras'      => 'ean13',
            'Precio'            => 'priceCatalog',
            'PrecioBase'        => 'priceWholesale',
            'Surtido'           => '',
            'Cantidad'          => 'stock',
            'StockReal'         => 'stockCatalog',
            'StockTeorico'      => 'stockToShow',
            'StockDisponible'   => 'stockAvailable',
            'VMD'               => ''
        ];

        $nameConverter = new AttributeXmlConverter($mapping);

        $normalizer = new ObjectNormalizer(null, $nameConverter);

        $serializer = new Serializer([$normalizer, new ArrayDenormalizer()], [new JsonEncoder()]);

        $productsSystem = $serializer->deserialize($json, 'App\Entity\ProductSystem[]', 'json');

        return $productsSystem;

    }

}