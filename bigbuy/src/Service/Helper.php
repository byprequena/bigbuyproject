<?php

namespace App\Service;

use Symfony\Component\Config\Definition\Exception\InvalidTypeException;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Response;

class Helper {

    private $filesystem;

    public function __construct(Filesystem $filesystem) {
        $this->fileSystem = $filesystem;
    }

    public function isJson($string): bool
    {
        json_decode($string);
        if(json_last_error() != JSON_ERROR_NONE) {
            throw new InvalidTypeException("The data is not a json");
        }
        else {
            return true;
        }
    }

    public function saveJson($data, $jsonDir): Response
    {
        $this->isJson($data);
    
        try {
         $this->fileSystem->dumpFile($jsonDir.'/file.json', $data);
        }
        catch(IOException $e) {
            throw $e;
        }

        return new Response('Json uploaded.', 200);
    }

}