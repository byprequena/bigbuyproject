<?php

namespace App\Entity;

use App\Model\ProductSystemInterface;
use App\Repository\ProductSystemRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProductSystemRepository::class)
 */
class ProductSystem implements ProductSystemInterface
{
    
    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=255)
     */
    private $sku;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $ean13;
    
    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $priceCatalog;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $priceWholesale;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $brandName;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $categoryName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $widthPackaging;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $heightPackaging;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $lengthPackaging;
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $weightPackaging;

    /**
     * @ORM\Column(type="object")
     */
    private $productAttributes;
    
    /**
     * @ORM\Column(type="array")
     */
    private $productImages = [];

    /**
     * @ORM\Column(type="object")
     */
    private $md5Attributes;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $stock;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $stockCatalog;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $stockToShow;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $stockAvailable;

    // Getters
    public function getSku(): ?string
    {
        return $this->sku;
    }

    public function getEan13(): ?string
    {
        return $this->ean13;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function getBrandName(): ?string
    {
        return $this->brandName;
    }

    public function getCategoryName(): ?string
    {
        return $this->categoryName;
    }

    public function getStock(): ?int
    {
        return $this->stock;
    }

    public function getStockCatalog(): ?int
    {
        return $this->stockCatalog;
    }

    public function getStockToShow(): int
    {
        return $this->stockToShow;
    }

    public function getStockAvailable(): int
    {
        return $this->stockAvailable;
    }

    public function getWidthPackaging(): ?string
    {
        return $this->widthPackaging;
    }

    public function getHeightPackaging(): ?string
    {
        return $this->heightPackaging;
    }

    public function getLengthPackaging(): ?string
    {
        return $this->lengthPackaging;
    }

    public function getWeightPackaging(): ?string
    {
        return $this->weightPackaging;
    }

    public function getProductAttributes()
    {
        return $this->productAttributes;
    }

    public function getProductImages(): array
    {
        return $this->productImages;
    }

    public function getMd5Attributes()
    {
        return $this->md5Attributes;
    }
    
    public function getPriceCatalog(): ?float
    {
        return $this->priceCatalog;
    }
    
    public function getPriceWholesale(): ?float
    {
        return $this->priceWholesale;
    }

    // Setters
    public function setSku(string $sku)
    {
        $this->sku = $sku;
    }

    public function setEan13(?string $ean13)
    {
        $this->ean13 = $ean13;
    }

    public function setDescription(?string $description)
    {
        $this->description = $description;
    }

    public function setBrandName(?string $brandName)
    {
        $this->brandName = $brandName;
    }

    public function setCategoryName(?string $categoryName)
    {
        $this->categoryName = $categoryName;
    }

    public function setStock(?int $stock)
    {
        $this->stock = $stock;
    }
    public function setStockCatalog(?int $stockCatalog)
    {
        $this->stockCatalog = $stockCatalog;
    }
    public function setStockToShow(?int $stockToShow)
    {
        $this->stockToShow = $stockToShow;
    }
    public function setStockAvailable(?int $stockAvailable)
    {
        $this->stockAvailable = $stockAvailable;
    }

    public function setWidthPackaging($widthPackaging)
    {
        $this->widthPackaging = $widthPackaging;
    }

    public function setHeightPackaging($heightPackaging)
    {
        $this->heightPackaging = $heightPackaging;
    }

    public function setLengthPackaging($lengthPackaging)
    {
        $this->lengthPackaging = $lengthPackaging;
    }

    public function setWeightPackaging($weightPackaging)
    {
        $this->weightPackaging = $weightPackaging;
    }

    public function setProductAttributes($productAttributes)
    {
        $this->productAttributes = $productAttributes;
    }

    public function setProductImages(array $productImages)
    {
        $this->productImages = $productImages;
    }

    public function setMd5Attributes($md5Attributes)
    {
        $this->md5Attributes = $md5Attributes;
    }

    public function setPriceCatalog($priceCatalog)
    {
        $this->priceCatalog = $priceCatalog;
    }

    public function setPriceWholesale($priceWholesale)
    {
        $this->priceWholesale = $priceWholesale;
    }

}