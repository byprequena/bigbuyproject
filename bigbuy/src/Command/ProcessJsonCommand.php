<?php

namespace App\Command;

use App\Service\Json\ProcessJson;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ProcessJsonCommand extends Command
{
    private $processJson;

    protected static $defaultName = 'process-json';
    protected static $defaultDescription = 'Add a short description for your command';

    public function __construct(ProcessJson $processJson) 
    {
        $this->processJson = $processJson;

        parent::__construct();

    }

    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $this->processJson->process();

        $io->success('Json processed.');

        return Command::SUCCESS;
    }
}
