<?php

namespace App\Command;

use App\Service\Csv\ProcessCsv;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ProcessCsvCommand extends Command
{
    private $processCsv;

    protected static $defaultName = 'process-csv';
    protected static $defaultDescription = 'Add a short description for your command';

    public function __construct(ProcessCsv $processCsv) 
    {
        $this->processCsv = $processCsv;

        parent::__construct();

    }

    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $this->processCsv->process();

        $io->success('Csv processed.');

        return Command::SUCCESS;
    }
}
