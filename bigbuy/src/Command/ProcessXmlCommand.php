<?php

namespace App\Command;

use App\Service\Xml\ProcessXml;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ProcessXmlCommand extends Command
{
    private $processXml;

    protected static $defaultName = 'process-xml';
    protected static $defaultDescription = 'Add a short description for your command';

    public function __construct(ProcessXml $processXml) 
    {
        $this->processXml = $processXml;

        parent::__construct();

    }

    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $this->processXml->process();

        $io->success('Xml processed.');

        return Command::SUCCESS;
    }
}
