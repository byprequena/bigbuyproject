<?php

namespace App\Controller;

use App\Service\Helper;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class JsonController extends AbstractController
{

    private $jsonDir;
    private $helper;

    public function __construct(Helper $helper, $jsonDir)
    {
      $this->jsonDir = $jsonDir;
      $this->helper = $helper;
    }

    /**
     * @Route("/post-json", methods={"POST"}, name="postJson")
     */
    public function postAction(Request $request): Response
    {
      return $this->helper->saveJson($request->getContent(), $this->jsonDir);
    }

}
