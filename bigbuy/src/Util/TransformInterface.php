<?php

namespace App\Util;

/**
 * Json, csv, xml
 */
interface TransformInterface {

    public function getArrayMd5(): Array;

    public function transform($type): Array;

    public function addMd5(Array $array1, Array $array2);

}