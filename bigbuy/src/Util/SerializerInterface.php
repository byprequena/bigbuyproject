<?php

namespace App\Util;

/**
 * Json, csv, xml
 */
interface SerializerInterface {

    public function deserialize($type): Array;

}