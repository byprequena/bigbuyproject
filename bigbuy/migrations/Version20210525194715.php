<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210525194715 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product_system CHANGE width_packaging width_packaging VARCHAR(255) DEFAULT NULL, CHANGE height_packaging height_packaging VARCHAR(255) DEFAULT NULL, CHANGE length_packaging length_packaging VARCHAR(255) DEFAULT NULL, CHANGE weight_packaging weight_packaging VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product_system CHANGE width_packaging width_packaging DOUBLE PRECISION DEFAULT NULL, CHANGE height_packaging height_packaging DOUBLE PRECISION DEFAULT NULL, CHANGE length_packaging length_packaging DOUBLE PRECISION DEFAULT NULL, CHANGE weight_packaging weight_packaging DOUBLE PRECISION DEFAULT NULL');
    }
}
