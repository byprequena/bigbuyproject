<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210520093317 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE product_attribute (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) DEFAULT NULL, value VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_system (id INT AUTO_INCREMENT NOT NULL, sku VARCHAR(255) NOT NULL, ean13 VARCHAR(255) DEFAULT NULL, description LONGTEXT DEFAULT NULL, brand_name VARCHAR(255) DEFAULT NULL, category_name VARCHAR(255) DEFAULT NULL, width_packaging DOUBLE PRECISION DEFAULT NULL, height_packaging DOUBLE PRECISION DEFAULT NULL, length_packaging DOUBLE PRECISION DEFAULT NULL, weight_packaging DOUBLE PRECISION DEFAULT NULL, product_images LONGTEXT NOT NULL COMMENT \'(DC2Type:simple_array)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE product_attribute');
        $this->addSql('DROP TABLE product_system');
    }
}
