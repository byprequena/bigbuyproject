<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210520094906 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product_attribute ADD product_system_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE product_attribute ADD CONSTRAINT FK_94DA5976BE359928 FOREIGN KEY (product_system_id) REFERENCES product_system (id)');
        $this->addSql('CREATE INDEX IDX_94DA5976BE359928 ON product_attribute (product_system_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product_attribute DROP FOREIGN KEY FK_94DA5976BE359928');
        $this->addSql('DROP INDEX IDX_94DA5976BE359928 ON product_attribute');
        $this->addSql('ALTER TABLE product_attribute DROP product_system_id');
    }
}
