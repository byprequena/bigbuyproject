<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210523135517 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product_system ADD price_catalog DOUBLE PRECISION DEFAULT NULL, ADD price_wholesale DOUBLE PRECISION DEFAULT NULL, ADD stock INT DEFAULT NULL, ADD stock_catalog INT DEFAULT NULL, ADD stock_to_show INT DEFAULT NULL, ADD stock_available INT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product_system DROP price_catalog, DROP price_wholesale, DROP stock, DROP stock_catalog, DROP stock_to_show, DROP stock_available');
    }
}
